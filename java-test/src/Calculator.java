
/**
 * Calculator command line application. A user runs it from the console and enters data through the console.
 * Calculator is able to perform operations of addition, subtraction, multiplication and division with two operands:
 * a + b, a - b, a * b, a / b
 * Calculator can work with Arabic and Roman numbers. An expression contains numbers of the same kind: 
 * II + 2 - is not a valid expression.
 * Calculator is able to work with Arabic numbers from 0 to 10 and with Roman ones from � to X.
 *
 */

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

	enum Parts {
		ARABIC, ROMAN, OPERATOR, INVALID
	}

	public static void main(String[] args) {

		greet();

		Scanner input = new Scanner(System.in);
		String str;

		while (true) {

			System.out.print(">>>");
			str = input.nextLine().toUpperCase();

			if ("EXIT".equals(str)) {
				input.close();
				System.out.println();
				System.out.println("Goodbye");
				System.exit(0);
			} else if (str.matches("\\p{javaWhitespace}*")) {
				continue;
			} else {
				evaluateExpression(str);
			}

		}
	}

	private static void greet() {
		System.out.println("Calculator");
		System.out.println();
		System.out.println("Evaluates expressions such as a + b, a - b, a * b, a / b");
		System.out.println("where a and b are Arabic numbers from 0 to 10");
		System.out.println("or Roman numbers from I to X");
		System.out.println("Both operands must be either Arabic or Roman");
		System.out.println("Input your expression after >>> and hit Enter");
		System.out.println("if you want to exit type 'exit'");
		System.out.println();
	}

	private static void evaluateExpression(String str) {

		ArrayList<String> parts = parseExpression(str);

		if (hasNoErrors(parts)) {

			System.out.println(evaluateParts(parts));
		}

	}

	private static ArrayList<String> parseExpression(String str) {

		ArrayList<String> parts = new ArrayList<String>();

		Pattern r = Pattern.compile("[0-9]+|[IVX]+|[-+/*]");
		Matcher m = r.matcher(str);

		while (m.find()) {
			parts.add(m.group());
		}

		return parts;
	}

	private static boolean hasNoErrors(ArrayList<String> parts) {

		if (parts.size() != 3) {
			System.out.println("Invalid expression. Please try again.");
			return false;
		} else if (detectPart(parts.get(0)) == Parts.ARABIC && detectPart(parts.get(2)) == Parts.ROMAN
				|| detectPart(parts.get(2)) == Parts.ARABIC && detectPart(parts.get(0)) == Parts.ROMAN) {
			System.out.println("Both operands must be either Arabic or Roman");
			return false;
		} else if ((detectPart(parts.get(0)) == Parts.ARABIC || detectPart(parts.get(0)) == Parts.ROMAN)
				&& (detectPart(parts.get(2)) == Parts.ARABIC || detectPart(parts.get(2)) == Parts.ROMAN)
				&& detectPart(parts.get(1)) == Parts.OPERATOR) {
			return true;
		} else {
			System.out.println("Invalid expression. Please try again.");
			return false;
		}
	}

	private static Parts detectPart(String str) {

		if (str.matches("[0-9]+")) {
			return Parts.ARABIC;
		} else if (str.matches("I|II|III|IV|V|VI|VII|VIII|IX|X")) {
			return Parts.ROMAN;
		} else if (str.matches("[-+/*]")) {
			return Parts.OPERATOR;
		} else {
			return Parts.INVALID;
		}
	}

	private static String evaluateParts(ArrayList<String> parts) {

		int a;
		int b;

		if (detectPart(parts.get(0)) == Parts.ROMAN) {
			a = convertToArabic(parts.get(0));
			b = convertToArabic(parts.get(2));
		} else {
			a = Integer.parseInt(parts.get(0));
			b = Integer.parseInt(parts.get(2));
		}

		char op = parts.get(1).charAt(0);

		switch (op) {
		case '+':
			return String.valueOf(a + b);
		case '-':
			return String.valueOf(a - b);
		case '*':
			return String.valueOf(a * b);
		case '/':
			if (b == 0) {
				return "Division by zero!";
			} else {
				return String.valueOf((double) a / b);
			}
		default:
			return "";
		}

	}

	private static int convertToArabic(String str) {
		if ("I".equals(str)) {
			return 1;
		} else if ("II".equals(str)) {
			return 2;
		} else if ("III".equals(str)) {
			return 3;
		} else if ("IV".equals(str)) {
			return 4;
		} else if ("V".equals(str)) {
			return 5;
		} else if ("VI".equals(str)) {
			return 6;
		} else if ("VII".equals(str)) {
			return 7;
		} else if ("VIII".equals(str)) {
			return 8;
		} else if ("IX".equals(str)) {
			return 9;
		} else if ("X".equals(str)) {
			return 10;
		} else {
			return 0;
		}
	}

}
